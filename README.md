[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

Avant tout
----------
**Le projet est actuellement en cours de développement. Il est livré sans test parce que bon, c'est long.**


Froxiss Reload
--------

Froxiss Reload est un agrégateur de flux RSS simple et auto hébergeable tels que [Leed] ou [Miniflux]. Il s'agit d'une version évoluée de **[Froxiss](https://framagit.org/Erase/Froxiss)**, entièrement refondue.

Simple d'installation et de paramétrage, il ne nécessite aucune base de données et se veut volontairement épuré en fonctionnalités. Un simple serveur web lui convient. Et un peu de PHP mais allez donc savoir à partir de quelle version tout cela fonctionne...

Froxiss est mono-utilisateur et ne requiert aucune compétence technique particulière.
 * Sans base de données
 * Script auto hébergeable
 * Gestion des flux RSS/ATOM
 * Gestion du multi-flux
 * Espace privé
 * Flux RSS global
 * Responsive Design
 * Options d'afichage des flux
 * Gestion de favoris


Installation
------------

Récupération des sources
```sh
$ git clone https://git.framasoft.org/Erase/Froxiss-Reload.git
```
ou en [téléchargeant l'archive zip]

Vérifier/assigner les droits en écriture au sein des répertoires :
 * `/system`
 * `/system/cache`
 * `/system/log`
 * `/system/data`
 * `/system/config`

Si votre installation se situe dans un sous-répertoire, Froxiss va tenter de le détecter et d'ajuster en conséquence la configuration. Dans le cas où une erreur subsiste, vous pouvez spécifier le nom de ce répertoire dans le fichier `/system/config/default.php`, en modifiant la ligne `$GLOBALS['TL_CONFIG']['path']`.

A l'aide de votre navigateur, se rendre sur la page du projet et renseigner un passphrase qui vous permettra de vous authentifier. 


Mise à jour
-----------
En règle général, il suffit simplement de remplacer les fichiers, les différents éléments de personnalisation et de configuration étant déportés & crées lors de l'installation. Maintenant, 'faudra pas hésiter à grogner si j'ai zappé un truc.


Informations secondaires
------------------------

Une partie des options de configuration est gérée directement dans l'espace `Paramètres`. Toutefois, d'autres options disponibles dans le fichier `/system/config/default.php` sont personnalisables en les dupliquant directement dans le fichier `/system/config/localconfig.php` puis en procédant aux modifications voulues.

Le répertoire `/data/` contient les différents paramètres utilisateurs (personnalisations des flux) et les favicons des différents sites. 
Le répertoire `/system/cache/` contient le contenu des flux RSS mis en cache.
Le répertoire `/system/logs/` contient d'éventuels logs PHP (selon paramétrage).


A suivre
--------
Evolutions à venir (en vrac) :
 * Optimisations CSS et PHP
 * Documentation et nettoyage PHP
 * Abonnement à des flux non RSS via [rss-bridge]
 * Gestion de mots clés et black word
 * Nettoyage amélioré des éléments en cache


Licence
-------

 En dehors des différentes licences spécifiques aux outils utilisés, le reste du code est distribué sous licence [Creative Commons BY-NC-SA 4.0]


Auteur
------
 * [Un simple développeur paysagiste] :) avec un peu de temps libre et aucune prétention - contact_at_green-effect.fr


Librairies annexes et outils utilisés
-------------------------------------

Pour faire fonctionner Froxiss, un ensemble d'outils ont été directement utilisés :
 * [SimplePie] : PHP script Feed parser
 * [FaviconDownloader] : PHP script find and download favicon
 * [Material Design]
 * [Goofi](https://github.com/broncowdd/goofi) : le super truc de **[Bronco](http://warriordudimanche.net/)**
 * Photo d'illustration Matt Lewis


Un grand merci donc aux différents créateurs et contributeurs ! Par ailleurs, de larges morceaux de code ou approches ont été empruntés au fabuleux CMS/CMF qu'est [Contao], distribué sous licence LGPL et mis au point par Léo Feyer.

[//]: # 

   [téléchargeant l'archive zip]: <https://git.framasoft.org/Erase/Froxiss-Reload/repository/archive.zip>
   [Miniflux]: <https://miniflux.net/>
   [Leed]: <https://github.com/ldleman/Leed>
   [Material Design]: <https://material.io/>
   [SimplePie]: <http://simplepie.org/>
   [FaviconDownloader]: <https://github.com/vincepare/FaviconDownloader>
   [Contao]: <https://contao.org/>
   [Creative Commons BY-NC-SA 4.0]: <http://creativecommons.org/licenses/by-nc-sa/4.0/>
   [rss-bridge]: <https://github.com/sebsauvage/rss-bridge>
   [Un simple développeur paysagiste]: <http://www.green-effect.fr>