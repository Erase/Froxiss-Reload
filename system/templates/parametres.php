<div class="mdl-grid portfolio-max-width portfolio-contact">
        <div class="mdl-cell mdl-cell--4-col mdl-card mdl-shadow--4dp">
        <div class="mdl-card__title">
            <h2 class="mdl-card__title-text">Paramètres</h2>
        </div>
        <div class="mdl-card__media">
            <img class="article-image" src="<?php echo TL_PATH; ?>/assets/images/page-bg.jpg" border="0" alt="">
        </div>
        <div class="mdl-card__supporting-text">
            <p>
                Par mesure de sécurité, la modification de certains paramètres techniques ne peut se faire qu'en modifiant les fichiers de configuration.
            </p>
            <?php
                if($n_version != ""){
                    echo $n_version;
                }
            ?>
            <form method="post" class="ce_form">
                <h3 class="mdl-cell mdl-cell--12-col mdl-typography--headline">Paramètres généraux</h3>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input type="text" pattern="[A-Z,a-z, \-]*" class="mdl-textfield__input" name="websiteTitle" id="websiteTitle" required="true" value="<?php echo $GLOBALS['TL_CONFIG']['websiteTitle'];?>" />
                    <label class="mdl-textfield__label" for="websiteTitle">Titre du site</label>                
                    <span class="mdl-textfield__error">Lettres, espaces et caractère "-" uniquement</span>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input type="text" required="true" class="mdl-textfield__input" name="passphrase" id="passphrase" value="<?php echo $GLOBALS['TL_CONFIG']['passphrase'];?>" />
                    <label class="mdl-textfield__label" for="passphrase">Passphrase</label>                
                    <span class="mdl-textfield__error">Ce champs ne peut être vide</span>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input type="text" required="true" class="mdl-textfield__input" name="keyStore" id="keyStore" value="<?php echo $GLOBALS['TL_CONFIG']['keyStore'];?>" />
                    <label class="mdl-textfield__label" for="keyStore">Clé de sécurité</label>                
                    <span class="mdl-textfield__error">Ce champs ne peut être vide</span>
                </div>
                <h3 class="mdl-cell mdl-cell--12-col mdl-typography--headline">Paramètres des flux</h3>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input type="text" pattern="[0-9]*" class="mdl-textfield__input" name="maxResultsPerBlock" id="maxResultsPerBlock" required="true" value="<?php echo $GLOBALS['TL_CONFIG']['maxResultsPerBlock'];?>" />
                    <label class="mdl-textfield__label" for="maxResultsPerBlock">Nombre d'articles à afficher</label>                
                    <span class="mdl-textfield__error">Chiffres uniquement</span>
                </div>
                <div class="mdl-checkbox__label mdl-js-checkbox__label">
                    <input type="checkbox" class="mdl-checkbox_input" name="enableCache" id="enableCache" value="true" <?php echo ($GLOBALS['TL_CONFIG']['enableCache'] ? ' checked="checked"' : '');?>/>
                    <label class="mdl-checkbox__label" for="enableCache">Activer le cache des flux</label>
                </div>
                <div class="mdl-checkbox__label mdl-js-checkbox__label">
                    <input type="checkbox" class="mdl-checkbox_input" name="showAttachement" id="showAttachement" value="true" <?php echo ($GLOBALS['TL_CONFIG']['showAttachement'] ? ' checked="checked"' : '');?>/>
                    <label class="mdl-checkbox__label" for="showAttachement">Afficher les images des items</label>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input type="text" pattern="[0-9]*" class="mdl-textfield__input" name="cacheDuration" id="cacheDuration" required="true" value="<?php echo $GLOBALS['TL_CONFIG']['cacheDuration'];?>" />
                    <label class="mdl-textfield__label" for="cacheDuration">Durée du cache (en secondes)</label>                
                    <span class="mdl-textfield__error">Chiffres uniquement</span>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input type="text" pattern="[0-9]*" class="mdl-textfield__input" name="maxDescription" id="maxDescription" value="<?php echo $GLOBALS['TL_CONFIG']['maxDescription'];?>" />
                    <label class="mdl-textfield__label" for="maxDescription">Nombre de caractères à afficher en description d'item (laisser vide si aucune limitation)</label>                
                    <span class="mdl-textfield__error">Chiffres uniquement</span>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input type="text" pattern="[0-9]*" required="true" class="mdl-textfield__input" name="sizeTitle" id="sizeTitle" value="<?php echo $GLOBALS['TL_CONFIG']['sizeTitle'];?>" />
                    <label class="mdl-textfield__label" for="sizeTitle">Taille des titres des items (en pixels)</label>                
                    <span class="mdl-textfield__error">Chiffres uniquement</span>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <textarea class="mdl-textfield__input" type="text" rows="5" id="flux" name="flux"><?php echo $liens;?></textarea>
                    <label class="mdl-textfield__label" for="flux">URL des flux RSS à suivre (1 flux par ligne)</label>
                </div>
                <?php
                    /* To do : pouvoir ajouter les restrictions par flux */
                    /*
                    <span data-lng>Je désire</span>
                    <select name="filter" id="filter">
                        <option value="show" data-lng>afficher</option>
                        <option value="hide" data-lng>masquer</option>
                    </select>
                    <span data-lng>en effectuant ma recherche sur</span>
                    <select name="where" id="where">
                        <option value="title" data-lng>le titre</option>
                        <option value="description" data-lng>la description</option>
                        <option value="all" data-lng>le titre et la description</option>
                    </select>
                    <span data-lng>les items contenant le(s) mot(s) clés (séparer chaque mot clé par une virgule) :</span>
                    <input type="text" name="words" value="<?php echo $GLOBALS['TL_CONFIG']['words'];?>" id="words">
                    */
                ?>
                <input type="hidden" name="id_form" value="config_form" />
                <p>
                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit">
                        Envoyer
                    </button>
                </p>
            </form>
        </div>
    </div>
</div>
