<div class="fav-container mdl-grid portfolio-max-width">
<?php
	if(count($_items)){
        foreach ($_items as $item){
            $o = Strings::generateRandom(10);
            ?>
            <div id="<?php echo $item['id'];?>" class="rss-item mdl-grid mdl-cell mdl-cell--2-col mdl-cell--4-col-tablet mdl-card mdl-shadow--4dp">
                <?php if(array_key_exists('favicon', $item) && $item['favicon'] != ""):?>
                    <img id="<?php echo $o;?>" src="<?php echo $item['favicon'];?>" class="fav" />
                    <div class="mdl-tooltip mdl-tooltip--left" for="<?php echo $o;?>" style="z-index:999999;">
                        <?php echo $item['source'];?>
                    </div>
                <?php endif;?>
                <a href="#" id="fa-<?php echo $o;?>" class="material-icons favoris remove>">star</a>
                <div class="mdl-tooltip mdl-tooltip--bottom" for="fa-<?php echo $o;?>" style="z-index:999999;">
                        Supprimer de mes favoris
                </div>
                <div class="mdl-card__title">
                    <h2 class="mdl-card__title-text" style="font-size:<?php echo (Config::get('sizeTitle') ? Config::get('sizeTitle') : 24);?>px"><?php echo $item['title'];?></h2>
                </div>
                <div class="mdl-card__supporting-text no-bottom-padding mdl-typography--text-right">
                	<span class="item-date"><?php echo $item['date'];?></span>
                </div>
                <div class="mdl-card__supporting-text mdl-typography--text-justify">
                    <?php if(Config::get('maxDescription') != "" && is_numeric(Config::get('maxDescription'))):?>
                        <p class="item-desc"><?php echo Strings::substr($item['desc'], Config::get('maxDescription')); ?></p>     
                    <?php else: ?>
                        <p class="item-desc"><?php echo $item['desc']; ?></p>
                    <?php endif; ?>                                   
                </div>
                <div class="mdl-card__actions mdl-card--border">
                    <a class="item-link mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect mdl-button--accent" href="<?php echo $item['lien'];?>">Lire l'article</a>
                </div>
            </div>
            <?php
        }
    }
?>
</div>
<script>
    jQuery(document).ready(function($) {
        $('a.favoris').click(function(){
            var $this = $(this);
            var $item = $this.parent('.rss-item');

            var _s = {
                'id': $item.attr('id'),
                'title': $item.find('h2').text(),
                'date' : $item.find('.item-date').html(),
                'desc' : $item.find('.item-desc').html(),
                'lien' : $item.find('.item-link').attr('href')
            };


            //~ Ajax, supprime le favoris
            $.ajax({
                type : "POST",
                data: {
                    action: 'removeFavoris',
                    _p: _s
                },
                dataType: 'json'
            }).done(function(ret){
                $item.fadeOut(400, function(){
                    $item.remove();
                    if($('.rss-item').length <= 0){
                        $('.fav-container ').html('<p style="display:block;margin:20px auto;">Aucun élément en favoris</p>');
                    }
                });
                var snackbarContainer = document.querySelector('#snackbar');
                var data = {
                  message: ret,
                  timeout: 1000
                };
                snackbarContainer.MaterialSnackbar.showSnackbar(data);

                
            });

            $this
                .removeClass('remove')
                .addClass('add')
                .text('star_border')
            .next('div')
                    .text('Ajouter à mes favoris');
            
        });

        if($('.rss-item').length <= 0){
            $('.fav-container ').html('<p style="display:block;margin:20px auto;">Aucun élément en favoris</p>');
        }
    });
</script>