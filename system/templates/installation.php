<div class="mdl-grid portfolio-max-width portfolio-contact">
        <div class="mdl-cell mdl-cell--4-col mdl-card mdl-shadow--4dp">
        <div class="mdl-card__title">
            <h2 class="mdl-card__title-text">Installation</h2>
        </div>
        <div class="mdl-card__media">
            <img class="article-image" src="<?php echo TL_PATH; ?>/assets/images/page-bg.jpg" border="0" alt="">
        </div>
        <div class="mdl-card__supporting-text">
            <p>
                Merci de renseigner les informations afin de procéder à l'installation.
            </p>
            <?php if(count($_dir_error)): ?>
                <p style="color:#d50000;">
                    Attention, les répertoires suivants nécessitent d'être accessibles en écriture :
                    <ul style="color:#d50000;">
                        <li>
                        <?php echo implode("</li><li>", $_dir_error);?>
                        </li>
                    </ul>
                </p>
            <?php endif;?>
            <form method="post" class="ce_form">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input type="text" required="true" class="mdl-textfield__input" name="passphrase" id="passphrase" value="" />
                    <label class="mdl-textfield__label" for="passphrase">Passphrase</label>                
                    <span class="mdl-textfield__error">Ce champs ne peut être vide</span>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <textarea class="mdl-textfield__input" type="text" rows="5" id="flux" name="flux"></textarea>
                    <label class="mdl-textfield__label" for="flux">URL des flux RSS à suivre <strong>(1 flux par ligne)</strong></label>
                </div>
                
                <input type="hidden" name="id_form" value="install_form" />
                <p>
                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit">
                        Envoyer
                    </button>
                </p>
            </form>
        </div>
    </div>
</div>
