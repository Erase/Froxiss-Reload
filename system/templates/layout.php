<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A portfolio template that uses Material Design Lite.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title><?php echo Config::get('websiteTitle');?></title>
    <link rel="stylesheet" href="<?php echo TL_PATH; ?>/system/vendor/goofi/goofi.php?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="<?php echo TL_PATH; ?>/assets/css/material.grey-pink.min.css" />
    <link rel="stylesheet" href="<?php echo TL_PATH; ?>/assets/css/styles.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="icon" type="image/png" href="<?php echo TL_PATH; ?>/assets/images/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?php echo TL_PATH; ?>/assets/images/favicon-16x16.png" sizes="16x16" />
    <script src="<?php echo TL_PATH; ?>/assets/js/jquery.min.js"></script>
</head>
<body>
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <header class="mdl-layout__header mdl-layout__header--waterfall portfolio-header">
            <div class="mdl-layout__header-row portfolio-logo-row">
                <span class="mdl-layout__title">
                    <div class="portfolio-logo"></div>
                    <span class="mdl-layout__title"><?php echo Config::get('websiteTitle');?></span>
                </span>
            </div>
            <?php if($islog) :?>
            <div class="mdl-layout__header-row portfolio-navigation-row mdl-layout--large-screen-only">
                <nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
                    <a class="mdl-navigation__link <?php echo ($currentPage == 'index' ? ' is-active' : '');?>" href="index.php">Accueil</a>
                    <a class="mdl-navigation__link <?php echo ($currentPage == 'favoris' ? ' is-active' : '');?>" href="index.php?page=favoris">Favoris</a>
                    <a class="mdl-navigation__link <?php echo ($currentPage == 'parametres' ? ' is-active' : '');?>" href="index.php?page=parametres">Paramètres</a>
                    <a class="mdl-navigation__link<?php echo ($currentPage == 'deconnexion' ? ' is-active' : '');?>" href="index.php?page=deconnexion">Se déconnecter</a>
                </nav>
            </div>
        	<?php endif;?>
        </header>
        <?php if($islog) :?>
        <div class="mdl-layout__drawer mdl-layout--small-screen-only">
            <nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
               <a class="mdl-navigation__link <?php echo ($currentPage == 'index' ? ' is-active' : '');?>" href="index.php">Accueil</a>
               <a class="mdl-navigation__link <?php echo ($currentPage == 'favoris' ? ' is-active' : '');?>" href="index.php?page=favoris">Favoris</a>
                <a class="mdl-navigation__link <?php echo ($currentPage == 'parametres' ? ' is-active' : '');?>" href="index.php?page=parametres">Paramètres</a>
                <a class="mdl-navigation__link<?php echo ($currentPage == 'deconnexion' ? ' is-active' : '');?>" href="index.php?page=deconnexion">Se déconnecter</a>
            </nav>
        </div>
        <?php endif;?>
        <main class="mdl-layout__content">
            <?php echo $content;?>
        </main>
        <footer class="mdl-mini-footer">
            <div class="mdl-mini-footer__left-section">
                <div class="mdl-logo">Froxiss - Minimalist RSS Reader - v<?php echo file_get_contents(TL_ROOT.'/system/version');?></div>
            </div>
            <div class="mdl-mini-footer__right-section">
                <ul class="mdl-mini-footer__link-list">
                    <li>Réalisé par <a href="https://www.green-effect.fr" title="Green Effect" target="_blank">Green Effect</a> sous CC BY-NC-SA 4.0</li>
                    <li><a href="https://framagit.org/Erase/Froxiss-Reload" target="_blank" title="Sources de Froxiss">Sources</a></li>
                </ul>
            </div>
        </footer>
        
    </div>
    
    <script src="<?php echo TL_PATH; ?>/assets/js/material.min.js"></script>
    <div id="snackbar" class="mdl-js-snackbar mdl-snackbar">
        <div class="mdl-snackbar__text"></div>
        <button class="mdl-snackbar__action" type="button"></button>
    </div>
    <?php if($_SESSION['message'] && $_SESSION['message'] != "") : ?>
		<script>	    
		    setTimeout(function(){
		  		var snackbarContainer = document.querySelector('#snackbar');
		        var data = {
		          message: "<?php echo $_SESSION['message'];?>",
		          timeout: 5000
		        };
		        snackbarContainer.MaterialSnackbar.showSnackbar(data);
			}, 1500);
		</script>
	<?php 
		unset($_SESSION['message']);
	endif;?>
    <script>
        jQuery(document).ready(function($){
            $('.mdl-layout').scroll(function(){
                if ($('.mdl-layout').scrollTop() >= 240) {
                   $('header').addClass('is-casting-shadow is-compact').css({
                        'position': 'fixed',
                        'min-height': 'auto'
                   }).find('.portfolio-logo-row').hide();
                }
                else {
                   $('header').removeClass('is-casting-shadow is-compact').css({
                        'position': 'relative',
                        'min-height': '64px'
                   }).find('.portfolio-logo-row').show();
                }
            });
        });  
    </script>
</body>

</html>

