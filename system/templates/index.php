<div class="mdl-grid portfolio-max-width">
<?php
	if(count($_items)){
        foreach ($_items as $item){
            $o = Strings::generateRandom(10);
            ?>
            <div id="<?php echo $item['key'];?>" class="rss-item mdl-grid mdl-cell mdl-cell--2-col mdl-cell--4-col-tablet mdl-card mdl-shadow--4dp">
                <?php if(array_key_exists('fav', $item) && $item['fav'] != ""):?>
                    <img id="<?php echo $o;?>" src="<?php echo $item['fav'];?>" class="fav" />
                    <div class="mdl-tooltip mdl-tooltip--left" for="<?php echo $o;?>" style="z-index:999999;">
                        <?php echo $item['source'];?>
                    </div>
                <?php else:?>
                    <span class="fav url"><?php echo $item['source'];?></span>
                <?php endif;?>
                <a href="#" id="fa-<?php echo $o;?>" class="material-icons favoris <?php echo ($item['favoris'] ? 'remove' : 'add');?>"><?php echo ($item['favoris'] ? 'star' : 'star_border');?></a>
                <div class="mdl-tooltip mdl-tooltip--bottom" for="fa-<?php echo $o;?>" style="z-index:999999;">
                        <?php echo ($item['favoris'] ? 'Supprimer de mes favoris' : 'Ajouter à mes favoris'); ?>
                </div>
                <div class="mdl-card__title">
                    <h2 class="mdl-card__title-text" style="font-size:<?php echo (Config::get('sizeTitle') ? Config::get('sizeTitle') : 24);?>px"><?php echo $item['title'];?></h2>
                </div>
                <?php if(Config::get('showAttachement')):?>
                    <?php if(array_key_exists('enclosure', $item) && $item['enclosure'] != ""):?>
                        <div class="mdl-card__media">
                            <a href="<?php echo $item['permalink'];?>"> 
                                <img class="article-image" src="<?php echo $item['enclosure'];?>" border="0" alt="">
                            </a>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
                <div class="mdl-card__supporting-text no-bottom-padding mdl-typography--text-right">
                	<span class="item-date">le <?php echo $item['date_read'];?></span>
                </div>
                <div class="mdl-card__supporting-text mdl-typography--text-justify">
                    <?php if(Config::get('maxDescription') != "" && is_numeric(Config::get('maxDescription'))):?>
                        <p class="item-desc"><?php echo Strings::substr($item['description'], Config::get('maxDescription')); ?></p>     
                    <?php else: ?>
                        <p class="item-desc"><?php echo $item['description']; ?></p>
                    <?php endif; ?>                                   
                </div>
                <div class="mdl-card__actions mdl-card--border">
                    <a class="item-link mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect mdl-button--accent" target="_blank" href="<?php echo $item['permalink'];?>">Lire l'article</a>
                </div>
            </div>
            <?php
        }
    }
?>
</div>
<script>
    jQuery(document).ready(function($) {
        $('a.favoris').click(function(){
            var $this = $(this);
            var $item = $this.parent('.rss-item');

            var _s = {
                'id': $item.attr('id'),
                'favicon': $item.find('.fav').attr('src'),
                'title': $item.find('h2').text(),
                'date' : $item.find('.item-date').html(),
                'desc' : $item.find('.item-desc').html(),
                'lien' : $item.find('.item-link').attr('href')
            };

            if($this.hasClass('add')){
                //~ Ajax, ajout du favoris
                $.ajax({
                    type : "POST",
                    data: {
                        action: 'addFavoris',
                        _p: _s
                    },
                    dataType: 'json'
                }).done(function(ret){
                    var snackbarContainer = document.querySelector('#snackbar');
                    var data = {
                      message: ret,
                      timeout: 1000
                    };
                    snackbarContainer.MaterialSnackbar.showSnackbar(data);
                });

                $this
                    .removeClass('add')
                    .addClass('remove')
                    .text('star')
                .next('div')
                    .text('Supprimer de mes favoris');
            }else{
                //~ Ajax, supprime le favoris
                $.ajax({
                    type : "POST",
                    data: {
                        action: 'removeFavoris',
                        _p: _s
                    },
                    dataType: 'json'
                }).done(function(ret){
                    var snackbarContainer = document.querySelector('#snackbar');
                    var data = {
                      message: ret,
                      timeout: 1000
                    };
                    snackbarContainer.MaterialSnackbar.showSnackbar(data);
                });

                $this
                    .removeClass('remove')
                    .addClass('add')
                    .text('star_border')
                .next('div')
                    .text('Ajouter à mes favoris');
            }
        });
    });
</script>