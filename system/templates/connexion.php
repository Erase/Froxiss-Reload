<div class="mdl-grid portfolio-max-width portfolio-contact">
        <div class="mdl-cell mdl-cell--4-col mdl-card mdl-shadow--4dp">
        <div class="mdl-card__media">
            <img class="article-image" src="<?php echo TL_PATH; ?>/assets/images/page-bg.jpg" border="0" alt="">
        </div>
        <div class="mdl-card__supporting-text">
            <form method="post" class="ce_form">
                <h3 class="mdl-cell mdl-cell--12-col mdl-typography--headline">Connexion</h3>
                
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input type="password" class="mdl-textfield__input" name="passphrase" id="passphrase" value="" />
                    <label class="mdl-textfield__label" for="passphrase">Passphrase</label>
                </div>
                <input type="hidden" name="id_form" value="connect_form" />
                <p>
                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit">
                        Se connecter
                    </button>
                </p>
            </form>
        </div>
    </div>
</div>