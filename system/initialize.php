<?php

	//~ Store the microtime
	define('TL_START', microtime(true));


	//~ Define the root path 
	define('TL_ROOT', dirname(__DIR__));

	//~ Include the helpers & Configuration
	require TL_ROOT.'/system/helper/functions.php';
	require TL_ROOT.'/system/config/constants.php';

	//~ Error handling
	ini_set('error_log', TL_ROOT.'/system/logs/error.log');

	//~ Include class Config
	require TL_ROOT.'/system/classes/Config.php';
	Config::preload();

	ini_set('display_errors', (Config::get('displayErrors') ? 1 : 0));
	error_reporting((Config::get('displayErrors') || Config::get('logErrors')) ? 1 : 0);

	//~ Include some classes required for further processing
	require TL_ROOT.'/system/classes/Utilities.php';
	require TL_ROOT.'/system/classes/Environment.php';
	require TL_ROOT.'/system/classes/Database.php';
	require TL_ROOT.'/system/classes/Input.php';
	require TL_ROOT.'/system/classes/Strings.php';
	require TL_ROOT.'/system/classes/Date.php';
	require TL_ROOT.'/system/classes/Feed.php';
	require TL_ROOT.'/system/classes/FeedItem.php';
	require TL_ROOT.'/system/classes/FeedReader.php';
	require TL_ROOT.'/system/classes/Favicon.php';
	require TL_ROOT.'/system/classes/Template.php';

	//~ Vendor
	require TL_ROOT.'/system/vendor/simplepie/autoloader.php';
	require TL_ROOT.'/system/vendor/favicondownloader/FaviconDownloader.class.php';

	//~ Define the relative path to the installation
	define('TL_PATH', Environment::get('url').'/'.Config::get('path'));

	//~ Agent info
	$objAgent = Environment::get('agent');

	//~ Check auth exept rss steam
	$isRSSRequest = false;
	if(Input::get('do') === 'rss'/* && !is_null(Input::get('u')) && Input::get('u') === Strings::simpleCrypt(Config::get('keyStore'))*/){
		$isRSSRequest = true;
	}


	//~ Start the session
    @session_set_cookie_params(0, (!is_null(TL_PATH) ? TL_PATH : '/')); 
	@session_start();


	//~ Get the Config instance
	$objConfig = Config::getInstance();

	//~ Set the website path 
	Config::set('websitePath', TL_PATH);

	//~ Set template folder
	Config::set('template_path', TL_ROOT.'/'.Config::get('template_path'));

	//~ Set page folder
	Config::set('page_path', TL_ROOT.'/'.Config::get('page_path'));

	//~ Set the timezone
	@ini_set('date.timezone', Config::get('timeZone'));
	@date_default_timezone_set(Config::get('timeZone'));


	//~ Define the datastore file
	Config::set('dataStorePath', TL_ROOT.'/system/data/');

	if(Config::get('keyStore') == ""){
		$objConfig->persist('keyStore', Strings::generateRandom(18));
        $objConfig->save();
	}

	Config::set('DataStoreFile', Config::get('dataStorePath').Strings::simpleCrypt(Config::get('keyStore')));


	//~ Set database reference
	$db = new Database();

	//~ Ajax request
	if(Environment::get('isAjaxRequest')){
		if(array_key_exists('action', $_REQUEST) && $_REQUEST['action'] !== ""){
			require TL_ROOT.'/system/classes/Ajax.php';
			$action = $_REQUEST['action'];
			if(array_key_exists('do', $_REQUEST)){
				unset($_REQUEST['do']);
			}
			if(array_key_exists('page', $_REQUEST)){
				unset($_REQUEST['page']);
			}
			unset($_REQUEST['action']);
			new Ajax($action, $_REQUEST);
		}
    }

    //~ Rss request
    if($isRSSRequest){
    	$_links = $db->get('data');
		
		//~ Load feeds
		$oFeeds = new FeedReader($_links);
		
		if(!is_null($oFeeds->objParseFeed)){
			$_items = array();
			$_c     = array();

  			foreach($oFeeds->objParseFeed->items as $item){
  				if(!empty($item['link']) && !in_array($item['link'], $_c)){
  					//~ Delete duplicate item by link
  					array_push($_c, $item['link']);

  					//~ Keywords filter
  					if((count(Config::get('_starWords')) && $item['show'] == 'star') 
  						OR (!count(Config::get('_starWords')) && $item['show'] == 'show')){
  						array_push($_items, $item);
  					}
  				}
  			}
		}

		//~ Generate RSS
		$rssfeed = '<?xml version="1.0" encoding="UTF-8"?>';
	   		$rssfeed .= '<rss version="2.0">';
	    		$rssfeed .= '<channel>';
	    			$rssfeed .= '<title>'.($oConf->title && !empty($oConf->title) ? $oConf->title : 'Flux personnalisé RSS Fusion').'</title>';
	    			$rssfeed .= '<link>'.($oConf->link && !empty($oConf->link) ? $oConf->link : 'https://framagit.org/Erase/RSS-Fusion-demo').'</link>';
					$rssfeed .= '<description>'.($oConf->description && !empty($oConf->description) ? $oConf->description : 'Flux généré avec RSS Fusion').'</description>';

	    if(count($_items)){
	    	foreach ($_items as $item){
	    		$rssfeed .= '<item>';
		    		$rssfeed .= '<title>'.html_entity_decode($item['title']).'</title>';
		    		$rssfeed .= '<link>'.$item['link'].'</link>';
		    		$rssfeed .= '<description><![CDATA['.$item['description'].']]></description>';
		    		$rssfeed .= '<pubDate>'.date('r', $item['pubdate']).'</pubDate>';
		    		$rssfeed .= '<category>'.$item['category'].'</category>';
		    		$rssfeed .= '<enclosure url="'.$item['enclosure'].'" />';

		    		$rssfeed .= '<permalink>'.$item['permalink'].'</permalink>';
		    		$rssfeed .= '<show>'.$item['show'].'</show>';
		    		$rssfeed .= '<base>'.$item['base'].'</base>';
	    		$rssfeed .= '</item>';
	    	}
	    }

	    		$rssfeed .= '</channel>';
    		$rssfeed .= '</rss>';


    	//~ Return RSS content
    	header('Content-type: application/xml');
		echo $rssfeed;
		exit;    	
    }

    
