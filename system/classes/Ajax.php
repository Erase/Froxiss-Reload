<?php

/**
 * Ajax
 *
 * Ajax miscellaneous method
 *
 */
class Ajax
{

	/**
	 * action
	 * @var array
	 */
	private $action;

	/**
	 * Call the method for $callAction
	 *
	 * @param string $callAction The method name
	 * @param array $_params The parameters
	 *
	 * @return mixed The method
	 */
	public function __construct($callAction, $_params = array()){
		$this->action = $callAction;

		if(method_exists($this, $this->action)){
			call_user_func_array(array($this, $this->action), $_params);
		}
		exit;
	}

	public function addFavoris($rss_item){
		global $db;
		
		$message = "Item ajouté aux favoris.";
		$_f = $db->getData()['favoris'];

		if(is_null($_f)){
			$_f = array();
		}

		if(array_key_exists($rss_item['id'], $_f)){
			$message = "Cet élément est déjà dans vos favoris.";
			
		}else{
			$_f[$rss_item['id']] = $rss_item;
			$db->set('favoris', $_f);
        	$db->save();
		}
		exit(json_encode(array($message)));
	}

	public function removeFavoris($rss_item){
		global $db;
		
		$message = "Item supprimé des favoris.";
		$_f = $db->getData()['favoris'];

		if(is_null($_f)){
			$_f = array();
		}

		if(!array_key_exists($rss_item['id'], $_f)){
			$message = "Cet élément n'est pas dans vos favoris.";
			
		}else{
			unset($_f[$rss_item['id']]);
			$db->set('favoris', $_f);
        	$db->save();
		}
		exit(json_encode(array($message)));
	}
	
}
