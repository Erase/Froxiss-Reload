<?php 

    $_links = $db->get('data');
    $_favicon = array();
    foreach($_links as $link){
        if(!array_key_exists($link, $_favicon)){

            if(!Favicon::isFaviconExist($link)){
                Favicon::getDistantFavicon($link);
            }

            if(Favicon::isFaviconExist($link)){
                $_k = parse_url($link);
                if(array_key_exists('host', $_k)){
                    $_favicon[$_k['host']] = Favicon::getFavicon($link);
                }
            }
        }
    }

    //~ Load feeds
    $oFeeds = new FeedReader($_links);
    
    if(!is_null($oFeeds->objParseFeed)){
        $_items = array();
        $_c     = array();

        $_f = $db->getData()['favoris'];
        if(is_null($_f)){
            $_f = array();
        }
        foreach($oFeeds->objParseFeed->items as $item){
            if(!empty($item['link']) && !in_array($item['link'], $_c)){
                //~ Delete duplicate item by link
                array_push($_c, $item['link']);

                $key = "item_".md5($item['title'].$item['date_read']);
                $item['key'] = $key;
                if(array_key_exists($key, $_f)){
                    $item['favoris'] = true;
                }else{
                    $item['favoris'] = false;
                }


                //~ Keywords filter                
                if((count(Config::get('_starWords')) && $item['show'] == 'star') 
                    OR (!count(Config::get('_starWords')) && $item['show'] == 'show')){

                    $fav = null;
                    if(array_key_exists('base', $item) && $item['base'] !== ""){
                        $_k = parse_url($item['base']);
                        if(array_key_exists('host', $_k)){
                            $item['fav']    = $_favicon[$_k['host']];
                            $item['source'] = $_k['host'];
                        }
                    }

                    array_push($_items, $item);
                }
                
            }
        }
    }


    $view->_items  = $_items;
    $view->currentPage    = 'index';
    $view->content = $view->render('index.php');
    $view->islog = true;
    echo $view->render('layout.php');

?>