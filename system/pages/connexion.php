<?php

	if(!is_null(Input::post('id_form')) && Input::post('id_form') === "connect_form"){
        $objConfig->read();
        $passphrase = $objConfig->get('passphrase');

        $_SESSION['message'] = "Passphrase incorrecte";

        if($passphrase == Input::post('passphrase')){
        	$_SESSION['log'] = true;
        	$_SESSION['message'] = "Bienvenue";      		    
        }

        Utilities::redirect(TL_PATH.'/index.php');
    }

    $view = new Template();
    $view->currentPage    = 'connexion';

    $view->content = $view->render('connexion.php');
    $view->islog = false;
    echo $view->render('layout.php');

?>