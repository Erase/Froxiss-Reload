<?php 
    //~ Vérifie si un sous-dossier d'installation a été spécifié selon le contexte
    $_out = array();
    preg_match("/\/(.)*\//", Environment::get('requestUri'), $_out);
    if(count($_out)){
        $uri = str_replace('/', '', $_out[0]);
        if($uri != Config::get('path')){
            $objConfig->persist('path', $uri);
            $objConfig->save();

            Utilities::reload();
        }
    }
    
    if(!is_null(Input::post('id_form')) && Input::post('id_form') === "install_form"){
        touch(TL_ROOT.'/system/config/localconfig.php');

        $_SESSION['message'] = 'Bienvenue sur '.Config::get('websiteTitle').' !';

        $objConfig->read();

        $_data_to_manage = array(
            'passphrase',
        );

        touch(TL_ROOT.'/system/install');

        foreach($_data_to_manage as $k){
            $post = $_POST[$k];
            if(array_key_exists($k, $_POST)){
                if($post === ""){
                    $objConfig->delete('$GLOBALS[\'TL_CONFIG\'][\''.$k.'\']');
                }else{
                    $objConfig->persist($k, $post);
                }
            }else{
                $objConfig->persist($k, 'false');
            }
            $objConfig->save();
        }

        //~ Flux
        if(array_key_exists('flux', $_POST) && !empty($_POST['flux'])){
            $flux = \Input::post('flux');
            $_links = array_unique(
                array_map('trim', 
                    preg_split( '/\r\n|\r|\n/', $flux)
                )
            );

            $_links_save = array();
            $_lien_off = array();
            foreach($_links as $link){
                if(!array_key_exists($link, $_links_save)){
                    if(!Favicon::isFaviconExist($link)){
                        Favicon::getDistantFavicon($link);
                    }

                    $_k = parse_url($link);
                    if(array_key_exists('host', $_k)){
                        array_push($_links_save, $link);
                    }else{
                        array_push($_lien_off, $link);
                    }
                }
            }

            if(count($_lien_off)){
                $_SESSION['message'] .= " Attention ! Les liens suivants ne sont pas valides : ".implode(', ', $_lien_off);
            }

            $db->set('data', $_links_save);
            $db->save();
        }     

        unset($_POST);

        $_SESSION['log'] = true;

        Utilities::reload();
    }
?>

<?php
    $_dir = array(
        "/system",
        "/system/cache",
        "/system/logs",
        "/system/data",
        "/system/config"
    );

    $_dir_error = array();
    foreach($_dir as $dir){
        if(!is_writable(TL_ROOT.$dir)){
            array_push($_dir_error, $dir);
        }
    }

    $view              = new Template();
    $view->currentPage = 'installation';
    $view->liens       = implode(PHP_EOL, $db->get('data'));
    $view->_dir_error  = $_dir_error;
    $view->content     = $view->render('installation.php');
    $view->islog       = false;
    echo $view->render('layout.php');

?>