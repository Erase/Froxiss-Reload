<?php 
    if(!is_null(Input::post('id_form')) && Input::post('id_form') === "config_form"){
        $_SESSION['message'] = 'Vos paramètres ont été enregistrés.';

        $objConfig->read();
        $_localData = $objConfig->arrData;

        $_data_to_manage = array(
            'websiteTitle',
            'passphrase',
            'keyStore',
            'maxResultsPerBlock',
            'enableCache',
            'cacheDuration',
            'showAttachement',
            'maxDescription',
            'sizeTitle'
        );

        foreach($_data_to_manage as $k){
            $post = $_POST[$k];
            if(array_key_exists($k, $_POST)){
                if($post === ""){
                    $objConfig->delete('$GLOBALS[\'TL_CONFIG\'][\''.$k.'\']');
                }else{
                    $objConfig->persist($k, $post);
                }
            }else{
                $objConfig->persist($k, 'false');
            }
            $objConfig->save();
        }


        //~ Flux
        if(array_key_exists('flux', $_POST) && !empty($_POST['flux'])){
            $flux = \Input::post('flux');
            $_links = array_unique(
                array_map('trim', 
                    preg_split( '/\r\n|\r|\n/', $flux)
                )
            );

            $_links_save = array();
            $_lien_off = array();
            foreach($_links as $link){
                if(!array_key_exists($link, $_links_save)){
                    if(!Favicon::isFaviconExist($link)){
                        Favicon::getDistantFavicon($link);
                    }

                    $_k = parse_url($link);
                    if(array_key_exists('host', $_k)){
                        array_push($_links_save, $link);
                    }else{
                        array_push($_lien_off, $link);
                    }
                }
            }

            if(count($_lien_off)){
                $_SESSION['message'] .= " Attention ! Les liens suivants ne sont pas valides : ".implode(', ', $_lien_off);
            }

            $db->set('data', $_links_save);
            $db->save();

            //~ Where to look
            /*if(array_key_exists('where', $_POST) && !empty($_POST['where'])){
                $where = Input::post('where');
                \Config::set('where', $where);
                $objConfig->persist('where', $where);
                $objConfig->save();
            }

            //~ Show or hide keywords
            if(array_key_exists('filter', $_POST) && !empty($_POST['filter'])){
                $mode = "_starWords";

                if(Input::post('filter') === 'hide'){
                    $mode = "_badWords";
                }

                if(!empty($_POST['words'])){
                    $_words = implode(',', array_unique(
                        array_map('trim', 
                            explode(',', Input::post('words'))
                        )
                    ));

                    Config::set($mode, $_words);
                    $objConfig->persist('words', $_words);
                    $objConfig->save();
                }
            }*/

        }     

        unset($_POST);
        
        Utilities::redirect(TL_PATH.'/index.php?page=parametres');
    }
?>

<?php
    //~ Check version
    $n_version = '';
    $current_version = file_get_contents(TL_ROOT.'/system/version');
    $distant_version = file_get_contents('https://framagit.org/Erase/Froxiss-Reload/raw/master/system/version');
    if($current_version != $distant_version){
        $n_version = "
            <p style='text-align:center;'>
                <strong>Une nouvelle version de Froxiss est disponible !</strong>
                <br />
                <a href='https://framagit.org/Erase/Froxiss-Reload/#mise-%C3%A0-jour' title='En savoir plus' target='_blank'>
                    En savoir plus
                </a>
            </p>
        ";
    }   


    $view = new Template();

    $view->currentPage    = 'parametres';
    $view->liens = implode(PHP_EOL, $db->get('data'));
    $view->n_version = $n_version;
    $view->content = $view->render('parametres.php');
    $view->islog = true;
    echo $view->render('layout.php');

?>