<?php 

    //~ Load favorite feeds
    $_f = $db->getData()['favoris'];
    if(is_null($_f)){
        $_f = array();
    }

    $view->_items  = $_f;
    $view->currentPage    = 'favoris';
    $view->content = $view->render('favoris.php');
    $view->islog = true;
    echo $view->render('layout.php');

?>