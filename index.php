<?php 
	session_start();

    require_once 'system/initialize.php';

    $page = (Input::get('page') && Input::get('page') != "" ? Input::get('page') : 'index');

    //~ Log ? 
    if(!isset($_SESSION['log']) ){
    	$page = "connexion";
    }

    //~ Install ?
    if(!file_exists(TL_ROOT.'/system/install')){
    	$page = "installation";
    }

    if(file_exists(Config::get('page_path').$page.'.php')){
        $view = new Template();
        include Config::get('page_path').$page.'.php';
    }else{
        //~ 404
        include Config::get('page_path').'404.php';
    }
    
    
    
?>